# OD-Shadow-Quill

> Authentication Library

`ODCognitoAuth` is a simple library that will manage your connection to AWS Cognito Servers

## Installation
- Install with [npm](https://www.npmjs.com/)

```
npm i @orcden/od-authentication
```
## Usage
```
import '@orcden/od-authentication';
```
```
<od-top-button offset=5></od-top-button>
```
    
## Attributes
| Attribute | Type | Default | Description                                                                             |
|-----------|---------|---------|-----------------------------------------------------------------------------------------|
| `offset`  | Number  | 1       | In pixels. Set to change how far down the page the user has to scroll before the button is active.  |
| `active`  | Boolean | false   | Controls CSS to show/hide the button. Setting offset to 0 sets the button to always active. |

## Functions
| Name | Parameters | Description                                  |
|-----------|------|-----------------------------------------------|
| `toTop`   | None | Scrolls the users screen to the top. |

## Styling
- CSS variables are available to alter the default styling provided

| CSS Variables                      |
|------------------------------------|
| `--od-top-button-height`           |
| `--od-top-button-width`            |
| `--od-top-button-color`            |
| `--od-top-button-background-color` |
| `--od-top-button-font-size`        |
| `--od-top-button-hover-bg-color`   |
| `--od-top-button-left`             |
| `--od-top-button-right`            |
| `--od-top-button-padding`          |
| `--od-top-button-z-index`          |
| `--od-top-button-outline`          |
| `--od-top-button-border`           |
| `--od-top-button-border-radius`    |

## Development
### Run development server and show demo

```
npm run demo
```

### Run linter

```
npm run lint
```

### Fix linter errors

```
npm run fix
```

### Run tests

```
npm run test
```

### Build for production

```
npm run build
```