const merge = require('webpack-merge');
const common = require('./common/webpack.common');
const nodeExternals = require('webpack-node-externals');

module.exports = merge(common, {
    mode: 'production',
    entry: {
        index: './lib/main.js'
    },
    output: {
        chunkFilename: '[name].chunk.js',
        library: "od-shadow-quill",
        libraryTarget: "umd"
    }
});